//
//  AppointmentsViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/16/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface AppointmentsViewController : SuperViewController
{
    IBOutlet UILabel *lblCurrentAppointmentDate;
    IBOutlet UILabel *lblCurrentAppointmentTime;
    
    IBOutlet UIView *viewAppointment;
    
    IBOutlet UILabel *lblLastAppointmentCaption;
    IBOutlet UILabel *lblLastAppointment;
    
    IBOutlet UIButton *btnASetAppointment;
    
    IBOutlet UILabel *lblMissedAppointment;

}

-(IBAction) doneAppointment;

@end
