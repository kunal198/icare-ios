//
//  NewMedicationViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Medication.h"
#import "SuperViewController.h"

@interface NewMedicationViewController : SuperViewController<UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIButton *btnName;
    IBOutlet UIButton *btnStartDate;
    IBOutlet UIButton *btnEndDate;
    IBOutlet UIButton *btnFrequency;
    NSDate *datepickerdate;
    int datepickertag;
    UIDatePicker *pickerView;
    IBOutlet UITableView *tblViewMedicationTimes;
    int selectmedrow;
    UIToolbar *toolBar;
    UIPickerView *picker;
    
    NSMutableArray *arrayMedicines;
    Medication *medication;
    id delegate;
}

@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) NSMutableArray *arrayMedicines;
@property (nonatomic, strong) Medication *medication;

-(IBAction) actionBtnName;
-(IBAction) actionBtnStartDate:(id)sender;
-(IBAction) actionBtnEndDate:(id)sender;
-(IBAction) actionBtnFrequency:(id)sender;
-(IBAction) actionFilter:(id)sender;
-(IBAction) saveMedication;

@end
