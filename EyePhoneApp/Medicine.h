//
//  Medicine.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/23/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Medicine : NSObject 
{
    NSInteger medicineId;
    NSString * name;
}
@property (nonatomic, assign) NSInteger medicineId;
@property (nonatomic, strong) NSString * name;

@end
