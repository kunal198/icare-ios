//
//  Doctor.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/21/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Doctor : NSObject
{
    NSInteger docId;
    NSString *name;
    NSString *imageName;
    NSString *description;
}

@property (nonatomic, assign) NSInteger docId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *description;

@end