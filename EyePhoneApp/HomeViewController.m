//
//  HomeViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/14/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "HomeViewController.h"
#import "MapViewController.h"
#import "AppDelegate.h"
#import "SVWebViewController.h"
#import "GMGridView.h"

#define SegueWebView @"SegueWebView"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.title = @"Eye Care Center of Kauai!";
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        // app already launched
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // This is the first launch ever
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Eye Care Kauai" message:@"By using this app you agree to the following Disclaimer and Disclosure" delegate:nil cancelButtonTitle:@"Disclaimer and Disclosure" otherButtonTitles:nil];
        alert.delegate = self;
        [alert show];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) actionCall
{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"+18082460051"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(IBAction) actionWebsite:(id) sender
{
    SVWebViewController *webViewController = [[SVWebViewController alloc] initWithAddress:@"http://ecckauai.com/"];
    webViewController.navigationController.navigationBar.tintColor= self.navigationController.navigationBar.tintColor;
    [self.navigationController pushViewController:webViewController animated:YES];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //You can access the tableviewcontroller using segue.destinationViewController
    //Maybe you want to set your model here.
    if ([segue.identifier isEqualToString:SegueWebView])
    {
        if (sender == btnWebsite)
        {
            MapViewController *vc  = segue.destinationViewController;
            vc.loadMapOrWebsite = kLoadWebsite;
        }
//        else if (sender == btnDoctors){
//            MapViewController *vc  = segue.destinationViewController;
//            vc.loadMapOrWebsite = kLoadDoctors;
//
//        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self performSegueWithIdentifier: @"DisclaimerSegue" sender: self];
    }
}


@end