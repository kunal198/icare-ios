//
//  LensChange.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/1/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LensChange : NSObject
{
    NSDate *dateLensChange;
    NSInteger durationForLensChange;
}

@property (strong, nonatomic)   NSDate *dateLensChange;
@property (assign, nonatomic)   NSInteger durationForLensChange;

@end
