//
//  HomeViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/14/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface HomeViewController : SuperViewController <UIAlertViewDelegate>
{
    IBOutlet UIButton *btnWebsite;
    IBOutlet UIButton *btnDoctors;
}

-(IBAction) actionCall;
-(IBAction) actionWebsite:(id) sender;

@end
