//
//  ToolsViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/19/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
#import "SuperViewController.h"
@interface ToolsViewController : SuperViewController <MWPhotoBrowserDelegate>
{
    NSMutableArray *photos;
}

@property (nonatomic, strong) NSMutableArray *photos;

-(IBAction) actionAmslerGrid;
-(IBAction) actionVideos;
-(IBAction) actionSnellenChart;

@end