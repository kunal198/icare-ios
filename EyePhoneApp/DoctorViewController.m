//
//  DoctorViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/18/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "DoctorViewController.h"

@interface DoctorViewController ()

@end

@implementation DoctorViewController
@synthesize imgView, txtView, doc;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
    self.imgView.image = [UIImage imageNamed:self.doc.imageName];
    self.txtView.text = self.doc.description;
    
    self.navigationItem.title = self.doc.name;
    
    
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
