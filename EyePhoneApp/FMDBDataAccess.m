//
//  FMDBDataAccess.m
//  Books
//
//  Created by Arsalan Habib on 6/30/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "FMDBDataAccess.h"
#import "Medicine.h"
#import "Doctor.h"
#import "Video.h"

@implementation FMDBDataAccess

-(BOOL) addMedication:(Medication *) medication {
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *startDate = [outputFormatter stringFromDate:medication.dateStart];
    NSString *endDate;
    BOOL result;
    if (medication.dateEnd) {
        endDate =[outputFormatter stringFromDate:medication.dateEnd];
         result = [db executeUpdate:[NSString stringWithFormat:@"Insert into Medications (MedicineId, StartDate, EndDate) Values (%d, '%@', '%@')",medication.medicine.medicineId, startDate, endDate]];
    }
    else{
        result = [db executeUpdate:[NSString stringWithFormat:@"Insert into Medications (MedicineId, StartDate) Values (%d, '%@')",medication.medicine.medicineId, startDate]];

    }
    
    int rowid;
    if (result) {
        rowid = [db lastInsertRowId];
        
        NSLog(@"%d",rowid);
        
        
        
        for (NSDate *date in medication.arrayMedicationTimes) {
            NSString *strDate = [outputFormatter stringFromDate:date];
            BOOL dateResult = [db executeUpdate:[NSString stringWithFormat:@"Insert into MedicationTime (MedicationId,Time) Values (%d,'%@')",rowid,strDate]];
        }
        
    }
    
    [db close];
    
    return rowid;
}

-(NSMutableArray *) getMedicines{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:[NSString stringWithFormat:@"Select * from Medicines"]];
    
    while ([results next]) {
        Medicine *med = [[Medicine alloc] init];
        med.medicineId = [results intForColumn:@"_id"];
        med.name = [results stringForColumn:@"MedicineName"];
        [array addObject: med];
    }
    [db close];
    return array;
}

-(NSMutableArray *) getAllMedications{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:[NSString stringWithFormat:@"Select * from Medications"]];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    while ([results next]) {
        Medication *med = [[Medication alloc] init];
        med.medicationId =[results intForColumn:@"_id"];
        med.medicine.medicineId = [results intForColumn:@"MedicineId"];
        med.dateLastTaken = [outputFormatter dateFromString:[results stringForColumn:@"LastTakenTime"]];
        med.dateStart =[outputFormatter dateFromString:[results stringForColumn:@"StartDate"]];
        med.dateEnd = [outputFormatter dateFromString:[results stringForColumn:@"EndDate"]];
        
        //Get Medicine Name
        FMResultSet *resultMedicineName = [db executeQuery:[NSString stringWithFormat:@"Select MedicineName from Medicines Where _id = %d", [results intForColumn:@"MedicineId"]]];
        
        while ([resultMedicineName next]) {
            med.medicine.name = [resultMedicineName stringForColumn:@"MedicineName"];
        }
        
        //Get Medication timings
        FMResultSet *resultMedicationTimings = [db executeQuery:[NSString stringWithFormat:@"Select * from MedicationTime Where MedicationId = %d", [results intForColumn:@"_id"]]];
        
        while ([resultMedicationTimings next]) {
            Event *medicationTime = [[Event alloc] init];
            medicationTime.dateEvent = [outputFormatter dateFromString:[resultMedicationTimings stringForColumn:@"Time"]];
            
            int finished = [resultMedicationTimings intForColumn:@"Finished"];
            
            if (finished == -1) {
                medicationTime.skipped = YES;
            }
            else{
                medicationTime.finished = [resultMedicationTimings boolForColumn:@"Finished"];
            }
                        
            medicationTime.eventId =[resultMedicationTimings intForColumn:@"_id"];
            [med.arrayMedicationTimes addObject:medicationTime];
        }
        
        //sort the array of medication times
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateEvent" ascending:TRUE];
        [med.arrayMedicationTimes sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                    
        [array addObject: med];
    }
    [db close];
    return array;
}

-(BOOL) markMedicationAsTaken:(Event *) event{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    BOOL result =  [db executeUpdate:[NSString stringWithFormat:@"Update MedicationTime set Finished = 1 where _id = %d",event.eventId]];

    [db close];
    
    return result;
}

-(BOOL) markMedicationAsSkipped:(Event *) event{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    BOOL result =  [db executeUpdate:[NSString stringWithFormat:@"Update MedicationTime set Finished = -1 where _id = %d",event.eventId]];
    
    [db close];
    
    return result;
}

-(BOOL) UpdateLastTakenForMedication:(Medication *) medication{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *lastTakenTime = [outputFormatter stringFromDate:[NSDate date]];
    
    BOOL result =  [db executeUpdate:[NSString stringWithFormat:@"Update Medications set LastTakenTime = '%@' where _id = %d ",lastTakenTime, medication.medicationId]];
    
    [db close];
    
    return result;
}

-(BOOL) resetAllMedications{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    BOOL result =  [db executeUpdate:@"Update MedicationTime set Finished = 0"];

    [db close];
    
    return result;

}

-(BOOL) deleteMedication:(Medication *) medication{

    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];

    
    BOOL result =  [db executeUpdate:[NSString stringWithFormat: @"Delete from MedicationTime where MedicationId = %d", medication.medicationId]];
    
    if (result) {
        result = [db executeUpdate:[NSString stringWithFormat:@"Delete from Medications where _id = %d", medication.medicationId]];
    }

    
    [db close];
    
    return result;
}

-(Doctor *) getDoctor:(int) docId{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:[NSString stringWithFormat:@"Select * from Doctors where _id = %d", docId]];
    Doctor *doc  = [[Doctor alloc] init];
    while ([results next]) {
        doc.docId = [results intForColumn:@"_id"];
        doc.name = [results stringForColumn:@"name"];
        doc.imageName = [results stringForColumn:@"Image"];
        doc.description = [results stringForColumn:@"Description"];
        [array addObject: doc];
    }
    
    [db close];
    return doc;
}

-(NSMutableArray *) getVideos
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:@"Select * from Videos"];
    
    
    
    while ([results next]) {
        Video *vid  = [[Video alloc] init];
        vid.videoId = [results intForColumn:@"_id"];
        vid.url = [NSURL URLWithString:[results stringForColumn:@"video"]];
        vid.title = [results stringForColumn:@"title"];
        vid.description = [results stringForColumn:@"description"];
        [array addObject: vid];
    }
    
    [db close];
    return array;
}

-(BOOL) medicationExist:(int) _id
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[appDelegate databasePath]];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:[NSString stringWithFormat: @"Select * from Medications where MedicineId = %d", _id]];
    
    BOOL resultExists = NO;
    
    while ([results next])
    {
        resultExists = YES;
    }
    
    [db close];
    
    return resultExists;
}

@end