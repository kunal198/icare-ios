//
//  Video.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 11/17/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Video : NSObject
{
    NSInteger videoId;
    NSURL *url;
    NSString *title;
    NSString *description;
}

@property (nonatomic, assign) NSInteger videoId;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *description;

@end
