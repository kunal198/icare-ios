//
//  UIImage+fixOrientation.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 10/2/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//



@interface UIImage (fixOrientation)
- (UIImage *)fixOrientation;

@end
