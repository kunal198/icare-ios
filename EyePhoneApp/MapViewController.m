//
//  MapViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/14/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "MapViewController.h"
#import "AppDelegate.h"



@interface MapViewAnnotation : NSObject <MKAnnotation> {
    
	NSString *title;
	CLLocationCoordinate2D coordinate;
    
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d;

@end


@implementation MapViewAnnotation

@synthesize title, coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d {
	if (self =[super init]){
	title = ttl;
	coordinate = c2d;
    }
	return self;
}


@end



@interface MapViewController ()

@end

@implementation MapViewController
@synthesize loadMapOrWebsite;
@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    NSURLRequest* request;
//    
//    if (loadMapOrWebsite == kLoadMap) {
//        
//        NSString *stringHtml = @"<html> <head> <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\"> <script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=true\"></script> <script type=\"text/javascript\"> var directionsDisplay = new google.maps.DirectionsRenderer(); var directionsService = new google.maps.DirectionsService(); function initialize() { var latlng = new google.maps.LatLng(%f, %f); var myOptions = { zoom: 15, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP }; var map = new google.maps.Map(document.getElementById(\"map_canvas\"), myOptions); directionsDisplay.setMap(map); calcRoute(); } function calcRoute() { var start = new google.maps.LatLng(%f, %f); var end = new google.maps.LatLng(%f, %f); var request = { origin:start, destination:end, travelMode: google.maps.DirectionsTravelMode.DRIVING }; directionsService.route(request, function(result, status) { if (status == google.maps.DirectionsStatus.OK) { directionsDisplay.setDirections(result); } }); } </script> </head> <body onload=\"initialize()\"> <div id=\"map_canvas\" style=\"width:300px; height:300px\"> </body> </html>";
//        NSString *routeString = [NSString stringWithFormat:stringHtml,37.3537,-122.0307,37.3537,-122.0307,38.156859,-121.533203];
//        
//        NSURL* nsUrl = [NSURL URLWithString:routeString];
//        
//        request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
//    }
//    else if(loadMapOrWebsite == kLoadWebsite){
//        NSString *string = @"http://www.ecckauai.com/";
//        
//        NSURL* nsUrl = [NSURL URLWithString:string];
//        
//        request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
//    }
//    else if(loadMapOrWebsite == kLoadDoctors){
//        NSString *string = @"http://ecckauai.com/aboutUs.php/";
//        
//        NSURL* nsUrl = [NSURL URLWithString:string];
//        
//        request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
//    }
//    
//    
//    
//    [webView loadRequest:request];
//    
//    webView.scalesPageToFit = YES;
    

    
    mapView.delegate = self;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
    self.navigationItem.title = @"Location";
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];

//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

// When a map annotation point is added, zoom to it (1500 range)
- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    CLLocationCoordinate2D location;
    location.latitude = (double) 21.968919;
    location.longitude = (double) -159.384446;
    
        MKCoordinateSpan zoom;
        zoom.latitudeDelta = .1f; //the zoom level in degrees
        zoom.longitudeDelta = .1f;//the zoom level in degrees
    
    
        MKCoordinateRegion myRegion;
        myRegion.center = location;//the location
        myRegion.span = zoom;//set zoom level
    
    
	MKAnnotationView *annotationView = [views objectAtIndex:0];
	id <MKAnnotation> mp = [annotationView annotation];

    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(location, 1000, 1000)];
    [mv setRegion:adjustedRegion animated:NO];
    [mv selectAnnotation:mp animated:YES];
    
    //the center of the region we'll move the map to
//    CLLocationCoordinate2D center;
//    center.latitude = (double) 21.968919;
//    center.longitude = (double) -159.384446;
//    
//    //set up zoom level
//    MKCoordinateSpan zoom;
//    zoom.latitudeDelta = .1f; //the zoom level in degrees
//    zoom.longitudeDelta = .1f;//the zoom level in degrees
//    
//    //stores the region the map will be showing
//    MKCoordinateRegion myRegion;
//    myRegion.center = center;//the location
//    myRegion.span = zoom;//set zoom level
//    
//    MKAnnotationView *annotationView = [views objectAtIndex:0];
//    id <MKAnnotation> mp = [annotationView annotation];
//    
//    //programmatically create a map that fits the screen
//    CGRect screen = [[UIScreen mainScreen] bounds];
//    MKMapView *mapViw = [[MKMapView alloc] initWithFrame:screen ];
//    
//    //set the map location/region
//    [mapViw setRegion:myRegion animated:YES];
//    
//    mapViw.mapType = MKMapTypeStandard;//standard map(not satellite)
//    
//    [self.view addSubview:mapView];//add map to the view
    
    
    
    

}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CLLocationCoordinate2D location;
    location.latitude = (double) 21.968919;
    location.longitude = (double)  -159.38444063454;

    
	// Add the annotation to our map view
	MapViewAnnotation *newAnnotation = [[MapViewAnnotation alloc] initWithTitle:@"Eye Care Center" andCoordinate:location];
	[self.mapView addAnnotation:newAnnotation];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction) actionDirections
{
    UIApplication *app = [UIApplication sharedApplication];
    
    NSString *coordinates = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=4439+Pahee+Street+Lihue,+HI++96766"];
    NSURL *url=[NSURL URLWithString: coordinates];
    [app openURL:url];
    
    
    
    
    
/*    // Check for iOS 6
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        
        
        // Create an MKMapItem to pass to the Maps app
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake(21.9715029, -159.3768849);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:@"Eye Care Center"];
        
        // Set the directions mode to "Walking"
        // Can use MKLaunchOptionsDirectionsModeDriving instead
        NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking};
        // Get the "Current User Location" MKMapItem
        MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
        // Pass the current location and destination map items to the Maps app
        // Set the direction mode in the launchOptions dictionary
        [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                       launchOptions:launchOptions];
    }*/
}

- (IBAction)supportPressed:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
        [composeViewController setMailComposeDelegate:self];
        [composeViewController setToRecipients:@[@"contact@ecckauai.com"]];
        [composeViewController setSubject:@"Eye Care Center"];
        [self presentViewController:composeViewController animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //Add an alert in case of failure
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
