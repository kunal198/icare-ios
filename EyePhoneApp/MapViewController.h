//
//  MapViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/14/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>
#import "SuperViewController.h"
@interface MapViewController : SuperViewController <MKMapViewDelegate,MFMailComposeViewControllerDelegate>
{
    NSInteger loadMapOrWebsite;
    
}
@property (nonatomic , strong ) IBOutlet MKMapView *mapView;
@property (nonatomic, assign) NSInteger loadMapOrWebsite;
-(IBAction) actionDirections;
- (IBAction)supportPressed:(id)sender;
@end
