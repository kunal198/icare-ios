//
//  DoctorsViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/18/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "DoctorsViewController.h"
#import "FMDBDataAccess.h"
#import "Doctor.h"
#import "DoctorViewController.h"
#define kSegueDoctorViewController @"SegueDoctorViewController"


@interface DoctorsViewController ()

@end

@implementation DoctorsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) actionShowDoctorInfo:(id) sender{
    UIButton *btn = (UIButton *) sender;
    
    FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
    Doctor *doc = [fmdb getDoctor:btn.tag];
    
    [self performSegueWithIdentifier:kSegueDoctorViewController  sender:doc];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //You can access the tableviewcontroller using segue.destinationViewController
    //Maybe you want to set your model here.
    if ([segue.identifier isEqualToString:kSegueDoctorViewController]) {
        
            DoctorViewController *vc  = segue.destinationViewController;
            vc.doc = (Doctor *) sender;
    }
}

@end