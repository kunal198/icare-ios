//
//  Medication.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "Medication.h"
#import "FMDBDataAccess.h"
@implementation Medication
@synthesize medicationId,name,dateStart,dateEnd,arrayMedicationTimes,medicine, dateLastTaken;

-(id) init{
    self = [super init];

    if (self) {
        self.medicationId =0;
        self.name = @"";
        self.dateStart = nil;
        self.dateEnd = nil;
        self.arrayMedicationTimes = [[NSMutableArray alloc] init];
        self.medicine = [[Medicine alloc] init];
    }
    
    return self;
}

-(void) markMedicationAsTaken{
    for (Event *event in self.arrayMedicationTimes){
        if (!event.finished) {
            FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
            [event setFinished:[fmdb markMedicationAsTaken:event]];
            goto outer;
        }
    }
outer:;
}

-(void) markMedicationAsSkipped{
    for (Event *event in self.arrayMedicationTimes){
        if (!event.skipped) {
            FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
            [event setSkipped:[fmdb markMedicationAsSkipped:event]];
            goto outer;
        }
    }
outer:;

}

-(void) recordLastTakenTime{
    FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
    [fmdb UpdateLastTakenForMedication:self];
    [self setDateLastTaken:[NSDate date]];
}

@end
