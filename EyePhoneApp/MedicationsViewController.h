//
//  MedicationsViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface MedicationsViewController : UITableViewController

{
    NSMutableArray *arrayMedications;
    IBOutlet UIBarButtonItem *btnEdit;
    int editing;
}

@property (strong, nonatomic) NSMutableArray *arrayMedications;
-(IBAction) edit;
-(IBAction) actionAddNewMedication;
@end
