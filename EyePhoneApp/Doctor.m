//
//  Doctor.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/21/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "Doctor.h"

@implementation Doctor

@synthesize docId;
@synthesize name;
@synthesize imageName;
@synthesize description;

@end