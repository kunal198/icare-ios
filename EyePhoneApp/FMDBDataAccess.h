//
//  FMDBDataAccess.h
//  Books
//
//  Created by Arsalan Habib on 6/30/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "AppDelegate.h"
#import "Medication.h"
#import "Event.h"
#import "Doctor.h"

@interface FMDBDataAccess : NSObject
{
    
}

-(NSMutableArray *) getMedicines;
-(int) addMedication:(Medication *) medication;
-(NSMutableArray *) getAllMedications;
-(BOOL) markMedicationAsTaken:(Event *) event;
-(BOOL) UpdateLastTakenForMedication:(Medication *) Medication;
-(BOOL)resetAllMedications;
-(BOOL) deleteMedication:(Medication *) medication;
-(BOOL)markMedicationAsSkipped:(Event *) event;
-(Doctor *) getDoctor:(int) docId;
-(NSMutableArray *) getVideos;
-(BOOL) medicationExist:(int) _id;
@end