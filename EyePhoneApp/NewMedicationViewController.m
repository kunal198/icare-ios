//
//  NewMedicationViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "NewMedicationViewController.h"
#import "FMDBDataAccess.h"

#define kName       1001
#define kStartDate  1002
#define kEndDate    1003
#define kFrequency  1004


@interface NewMedicationViewController ()

@end

@implementation NewMedicationViewController
@synthesize arrayMedicines, medication;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];

    self.arrayMedicines = [fmdb getMedicines]; //[[NSMutableArray alloc ] initWithObjects:@"AK-Pentolate",@"Acetazolamide",@"Acular",nil];
    NSLog(@"%@",self.arrayMedicines);
    self.medication = [[Medication alloc] init];
    tblViewMedicationTimes.delegate = self;
    tblViewMedicationTimes.dataSource = self;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    tblViewMedicationTimes.hidden = YES;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) actionBtnName{
    
}

-(IBAction) actionBtnStartDate:(id)sender{
    
}

-(IBAction) actionBtnEndDate:(id)sender{
    
}


-(IBAction) actionBtnFrequency:(id)sender{
    
}

-(IBAction)actionFilter:(id)sender{
    

    if (sender == btnName)
    {
        pickerView.hidden = true;
        [picker removeFromSuperview];
        [toolBar removeFromSuperview];

            picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,280,self.view.frame.size.width, 250)];
            toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,280,self.view.frame.size.width,44)];

    
        picker.delegate = self;
        picker.dataSource = self;
        [picker reloadAllComponents];
        picker.showsSelectionIndicator = YES;
        picker.tag = kName;
        [self.view addSubview:picker];
       toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,280,self.view.frame.size.width,44)];
        [toolBar setBarStyle:UIBarStyleBlackOpaque];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(donewithselectmedicine)];
        toolBar.items = @[barButtonDone];
        tblViewMedicationTimes.hidden = YES;

        barButtonDone.tintColor=[UIColor whiteColor];
        [self.view addSubview:toolBar];
    }
    else if(sender == btnStartDate || sender == btnEndDate || sender == btnFrequency){
        [pickerView removeFromSuperview];
        picker.hidden = true;
        [toolBar removeFromSuperview];
        pickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,280,self.view.frame.size.width, 250)];
        if (sender == btnStartDate) {
            pickerView.datePickerMode = UIDatePickerModeDate;
            [pickerView setMinimumDate: [NSDate date]];
            
            datepickertag = kStartDate;
        }
        else if (sender == btnEndDate){
            [pickerView removeFromSuperview];
            picker.hidden = true;
            [toolBar removeFromSuperview];
            pickerView.datePickerMode = UIDatePickerModeDate;
            [pickerView setMinimumDate: [NSDate date]];
            datepickertag = kEndDate;
        }
        else if (sender == btnFrequency){
            [pickerView removeFromSuperview];
            picker.hidden = true;
            [toolBar removeFromSuperview];
            pickerView.datePickerMode = UIDatePickerModeTime;
            datepickertag = kFrequency;
        }
        tblViewMedicationTimes.hidden = YES;

        [self.view addSubview:pickerView];
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,280,self.view.frame.size.width,44)];
        [toolBar setBarStyle:UIBarStyleBlackOpaque];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(donewithstartbtnselect)];
        toolBar.items = @[barButtonDone];
        barButtonDone.tintColor=[UIColor whiteColor];
        [self.view addSubview:toolBar];

    }
    
}

#pragma mark -
#pragma  UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [arrayMedicines count];
}

#pragma mark-
#pragma mark UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *string = [[self.arrayMedicines objectAtIndex:row] name];
    
    return string;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [picker selectRow:row inComponent:component animated:YES];
    selectmedrow = row;
    
}

-(void)donewithselectmedicine
{
    tblViewMedicationTimes.hidden = NO;
    pickerView.hidden = YES;
     picker.hidden = YES;
    [toolBar removeFromSuperview];
    int index = selectmedrow;
    NSString *name  = [[self.arrayMedicines objectAtIndex:index] name];
    self.medication.name = name;
    self.medication.medicine = [self.arrayMedicines objectAtIndex:index];
    [btnName setTitle:name forState: UIControlStateNormal];
   
}
-(void)donewithstartbtnselect
{
    if (datepickertag == kStartDate)
    {
       
           [toolBar removeFromSuperview];
           pickerView.hidden = YES;
           [picker removeFromSuperview];
           tblViewMedicationTimes.hidden = NO;
           
           self.medication.dateStart = pickerView.date;
           NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
           [dateFormatter setDateFormat: @"MMM-dd-yyyy"];
           NSString *dateString = [dateFormatter stringFromDate: pickerView.date];
           [btnStartDate setTitle:dateString forState: UIControlStateNormal];
   [btnStartDate setTitle:dateString forState: UIControlStateNormal];
    }
    
       
    
    else if (datepickertag == kEndDate)
    {
       
        
        [toolBar removeFromSuperview];
        pickerView.hidden = YES;
         [picker removeFromSuperview];
         tblViewMedicationTimes.hidden = NO;
    
        self.medication.dateEnd = pickerView.date;
            NSLog(@"%@",self.medication.dateStart);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat: @"MMM-dd-yyyy"];
        NSString *dateString = [dateFormatter stringFromDate: pickerView.date];
        [btnEndDate setTitle:dateString forState: UIControlStateNormal];
        }
   
        
    
    else if (datepickertag ==  kFrequency)
    {
         [picker removeFromSuperview];
         [toolBar removeFromSuperview];
        pickerView.hidden = YES;
        [self.medication.arrayMedicationTimes addObject:pickerView.date];
        tblViewMedicationTimes.hidden = NO;
        [tblViewMedicationTimes reloadData];
        
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [self.medication.arrayMedicationTimes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"h:mm a"];
    
    cell.textLabel.text = [outputFormatter stringFromDate:(NSDate*)[self.medication.arrayMedicationTimes objectAtIndex:indexPath.row]];

    
//    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image = [UIImage imageNamed:@"cross-icon.jpg"];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0,0,44,44)];
    btn.tag = indexPath.row;
    [btn addTarget:self action:@selector(removeTime:) forControlEvents:UIControlEventTouchUpInside];

    [cell setAccessoryView:btn];
    
        
    return cell;
}



/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Custom Methods

-(void) removeTime:(id) sender{
    UIButton *btn = (UIButton *) sender;
    
    int tag = btn.tag;
    
    [self.medication.arrayMedicationTimes removeObjectAtIndex:tag];
    [tblViewMedicationTimes reloadData];
}

-(IBAction) saveMedication
{
        NSString *alertText = @"";

    if (medication.name.length == 0) {
        alertText = @"Please select a Medicine";
    }
    else if(medication.dateStart == nil )
    {
        alertText = @"Start Date cannot be null";
    }

    else if (medication.dateEnd != nil) {
        if ([medication.dateStart  compare:medication.dateEnd] != NSOrderedSame)
        {
          if([medication.dateStart compare:medication.dateEnd] != NSOrderedAscending)
          {
              alertText = @"End Date cannot be later than Start Date";
          }
        }
        
        
    }
    else if(medication.arrayMedicationTimes.count == 0){
        alertText = @"Please select the time for this medication";
    }
    
    if (alertText.length != 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:alertText delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
        [alert show];

        return;
    }
    
    
    FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
    
    if ([fmdb medicationExist:medication.medicine.medicineId])
    {        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Medication already exists for this medicine!" delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    int medicationId = [fmdb addMedication:medication];
    
    for (NSDate *date in medication.arrayMedicationTimes) {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
        NSDictionary  *dictionary = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:medicationId],nil] forKeys:[NSArray arrayWithObjects:@"medication",nil] ];
        localNotification.userInfo = dictionary;
        localNotification.soundName=UILocalNotificationDefaultSoundName;
        [localNotification setFireDate:date]; //Set the date when the alert will be launched using the date adding the time the user selected on the timer
        [localNotification setTimeZone: [NSTimeZone localTimeZone]];
        [localNotification setRepeatInterval: NSDayCalendarUnit];
        [localNotification setAlertAction:@"EyePhoneApp"]; //The button's text that launches the application and is shown in the alert
        [localNotification setAlertBody:[NSString stringWithFormat: @"Time to take %@!", medication.medicine.name]]; //Set the message in the notification from the textField's text
        [localNotification setHasAction: YES]; //Set that pushing the button will launch the application
        [localNotification setApplicationIconBadgeNumber:[[UIApplication sharedApplication] applicationIconBadgeNumber]+1]; //Set the Application Icon Badge Number of the application's icon to the current Application Icon Badge Number plus 1
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification]; //Schedule the notification with the system
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Medication saved." delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
    [alert show];

    
    [self.navigationController  popViewControllerAnimated:YES];
    
}

-(IBAction) actionAddNewMedication{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}

@end