//
//  NewAppointmentViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Appointment.h"
#import "SuperViewController.h"

@interface NewAppointmentViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate>
{
    IBOutlet UILabel *lblCurrentAppointmentDate;
    IBOutlet UILabel *lblCurrentAppointmentTime;
    UIDatePicker *pickerView ;
    IBOutlet UIButton *btnReminder1;
    IBOutlet UIButton *btnReminder2;
    UIPickerView *picker;
    int pickerype ; 
    IBOutlet UIButton *btnSetAppointment;
    
    IBOutlet UIButton *btnSetYear;
    
     UIToolbar *toolBar;
    Appointment *appointment;
    
    NSMutableArray *years;
}
@property (strong, nonatomic) NSMutableArray *years;
@property (strong, nonatomic) Appointment *appointment;

-(IBAction) done;
-(IBAction) close;

-(IBAction)actionFilter:(id)sender;
@end
