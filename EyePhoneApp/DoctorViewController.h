//
//  DoctorViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/18/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Doctor.h"
#import "SuperViewController.h"
@interface DoctorViewController : SuperViewController
{
    Doctor *doc;
    
}
@property (nonatomic, strong) Doctor *doc;
@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) IBOutlet UITextView *txtView;

@end