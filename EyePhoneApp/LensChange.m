//
//  LensChange.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/1/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "LensChange.h"

@implementation LensChange
@synthesize dateLensChange, durationForLensChange;

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.dateLensChange forKey:@"dateLensChange"];
    [encoder encodeObject:[NSNumber numberWithInteger:self.durationForLensChange] forKey:@"durationForLensChange"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.dateLensChange = [decoder decodeObjectForKey:@"dateLensChange"];
        self.durationForLensChange = [[decoder decodeObjectForKey:@"durationForLensChange"] integerValue];
    }
    return self;
}   


@end
