//
//  LensesViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/1/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface LensesViewController : SuperViewController<UIPickerViewDataSource, UIPickerViewDelegate, UIActionSheetDelegate>
{
    IBOutlet UILabel * lblTime;
    IBOutlet UIButton * btnSetFrequency;
    IBOutlet UIButton * btnChangeLens;
    UIPickerView *pickerView;
    UIToolbar *toolBar;
    IBOutlet UILabel *LastChanged;
    
    NSDictionary *dictDurations;
    
    NSTimer *countdownTimer;
    
    IBOutlet UILabel *lblLensChangeMissed;
}

@property (nonatomic, strong)NSTimer *countdownTimer;
@property (nonatomic, strong) NSDictionary *dictDurations;
-(IBAction) actionSetFrequence;
-(IBAction) actionChangeLens;


@end