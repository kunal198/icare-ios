//
//  Event.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/27/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Event : NSObject
{
    NSDate *dateEvent;
    BOOL finished;
    BOOL skipped;
    NSInteger eventId;
}
@property (nonatomic, assign) BOOL skipped;
@property (nonatomic, strong) NSDate *dateEvent;
@property (nonatomic, assign) BOOL finished;
@property (nonatomic, assign) NSInteger eventId;

@end
