//
//  AppointmentsViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/16/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "AppointmentsViewController.h"
#import "Appointment.h"

@interface AppointmentsViewController ()

@end

@implementation AppointmentsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeTextColor : [UIColor whiteColor]};

}

- (void)viewWillAppear:(BOOL)animated
 {
     [lblMissedAppointment setHidden:YES];
     Appointment *appointment = [self loadAppointmentWithKey:@"CurrentAppointment"];
     
     if (appointment) {

         viewAppointment.hidden = NO;
         
         NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
         
         [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
         
         [dateFormatter setDateFormat: @"MMM-dd-yyyy"];
         
         NSString *dateString = [dateFormatter stringFromDate: appointment.dateAppointment];
         
         lblCurrentAppointmentDate.text = dateString;
         
         [dateFormatter setDateFormat: @"hh:mm a"];
         
         NSString *stringTime = [dateFormatter stringFromDate: appointment.dateAppointment];
         
         lblCurrentAppointmentTime.text= stringTime;
         
         NSDate *now = [NSDate date];
         
         if ([now compare:appointment.dateAppointment] == NSOrderedDescending){
             [lblMissedAppointment setHidden:NO];
         }
     }
     else{
         viewAppointment.hidden = YES;
     }
     
     [self loadLastAppointment];
     
     [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
     
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (Appointment *)loadAppointmentWithKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [defaults objectForKey:key];
    Appointment *obj = (Appointment *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
    return obj;
}

-(void) saveLastAppointment:(Appointment *) appointment
{
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:appointment];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:myEncodedObject forKey:@"LastAppointment"];
    [defaults synchronize];

//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

-(IBAction) doneAppointment
{
    Appointment  *appointment = [self loadAppointmentWithKey:@"CurrentAppointment"];
    [self saveLastAppointment:appointment];
    
    [self loadLastAppointment];
    [viewAppointment setHidden:YES];
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"CurrentAppointment"]];
        if ([uid isEqualToString:@"CurrentAppointment"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            break;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentAppointment"];
}

-(void) loadLastAppointment
{
    Appointment *appointment = [self loadAppointmentWithKey:@"LastAppointment"];
    
    if (appointment)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        [dateFormatter setDateFormat: @"MMM-dd-yyyy, hh:mm a"];
        
        NSString *dateString = [dateFormatter stringFromDate: appointment.dateAppointment];
        
        lblLastAppointment.text = dateString;
        
        lblLastAppointment.hidden = NO;
        lblLastAppointmentCaption.hidden = NO;
    }
    else{
        lblLastAppointment.hidden = YES;
        lblLastAppointmentCaption.hidden = YES;
    }
    
}

@end
