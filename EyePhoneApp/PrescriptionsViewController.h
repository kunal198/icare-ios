
//
//  PrescriptionsViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 10/1/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
#import "SuperViewController.h"
@interface PrescriptionsViewController : SuperViewController<MWPhotoBrowserDelegate>
{
    NSMutableArray *photos;
}

@property (nonatomic, strong) NSMutableArray *photos;
- (IBAction)btnTakePicture_Clicked:(id)sender;
- (IBAction)editingSwitchChanged:(UISwitch *)control;
@end
