//
//  SuperViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 1/27/14.
//  Copyright (c) 2014 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_OS_5_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@interface SuperViewController : UIViewController

@end
