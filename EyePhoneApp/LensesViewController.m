//
//  LensesViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/1/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "LensesViewController.h"
#import "LensChange.h"


@interface LensesViewController ()

@end

@implementation LensesViewController
@synthesize dictDurations, countdownTimer;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.dictDurations = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1",@"14",@"30", @"120",nil] forKeys: [NSArray arrayWithObjects: @"Daily", @"2 weeks", @"Monthly", @"Quarterly", nil ] ];
    
   
    
    LensChange *lensChange = [self loadLensChangeObjectWithKey:@"LastLensChange"];
    
    if (lensChange)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        [dateFormatter setDateFormat: @"MMM-dd-yyyy"];
        
        NSString *stringTime = [dateFormatter stringFromDate: lensChange.dateLensChange];
        
        LastChanged.text= stringTime;
    }

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
    self.title = @"Lenses";
    
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver: UIApplicationDidBecomeActiveNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(loadCountdownTimer)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];

    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeTextColor : [UIColor whiteColor]};

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[NSNotificationCenter defaultCenter] removeObserver: UIApplicationDidBecomeActiveNotification];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadCountdownTimer];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

-(IBAction) actionSetFrequence{
    
   pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,280,self.view.frame.size.width, 250)];
    pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    [self.view addSubview:pickerView];
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,280,self.view.frame.size.width,44)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(Donewithfrequency)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor whiteColor];
    [self.view addSubview:toolBar];
    LastChanged.hidden = true;

}


-(void)Donewithfrequency
{
    LastChanged.hidden = false;
    pickerView.hidden = true;
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"cancel"];

    [toolBar removeFromSuperview];
    NSString *strDuration = [[dictDurations allKeys] objectAtIndex:[pickerView selectedRowInComponent:0]];
    
    int daysToAdd = [[dictDurations objectForKey:strDuration] integerValue];
    
    [self setLensChangeReminder:daysToAdd];
    
    lblTime.textColor = [UIColor blackColor];
}

- (IBAction)cancellens:(id)sender {
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    [self.countdownTimer invalidate];
    lblTime.text = @"";
    self.countdownTimer=nil;
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"cancel"];
    for (int i=0; i<[eventArray count]; i++)
    {
        lblTime.text = @"";
        [self.countdownTimer invalidate];
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"CurrentLensChange"]];
        if ([uid isEqualToString:@"CurrentLensChange"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            break;
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"LastLensChange"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LastLensChange"];
    
}



-(IBAction) actionChangeLens{
    //reset
    LensChange *lensChange = [self loadLensChangeObjectWithKey:@"CurrentLensChange"];
    lensChange.dateLensChange = [NSDate date];
    [self saveLensChangeObject:lensChange withKey:@"LastLensChange"];
    [self setLensChangeReminder:lensChange.durationForLensChange];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    [dateFormatter setDateFormat: @"MMM-dd-yyyy"];
    
    NSString *stringTime = [dateFormatter stringFromDate: [NSDate date]];
    
    LastChanged.text= stringTime;
    lblTime.textColor = [UIColor blackColor];

}

#pragma mark -
#pragma  UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 4;
}

#pragma mark-
#pragma mark UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSArray *arrayKeys = [dictDurations allKeys];
    return [arrayKeys objectAtIndex:row];
}

#pragma mark-


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [pickerView selectRow:row inComponent:component animated:YES];
}



-(void) setLensChangeReminder:(int) daysToAdd{
    
    LensChange *lensChange = [[LensChange alloc] init];
    
    lensChange.durationForLensChange = daysToAdd;
    
    NSDate *now = [NSDate date];
    
    NSDate *newDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
//    NSDate *newDate = [now dateByAddingTimeInterval:60*daysToAdd];
    
    [self setupLensChangeNotifForDate:newDate];
    
    lensChange.dateLensChange = newDate;
    
    NSLog (@"");
    
    [self saveLensChangeObject:lensChange withKey:@"CurrentLensChange"];
    
    [self loadCountdownTimer];
}

-(void) loadCountdownTimer{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"cancel"]) {
        
    
    LensChange *lensChange = [self loadLensChangeObjectWithKey:@"CurrentLensChange"];
    
    if ([lensChange.dateLensChange compare:[NSDate date]] == NSOrderedAscending || [lensChange.dateLensChange compare:[NSDate date]] == NSOrderedSame) {
        [self showLateNotification];

        return;
        
    }
    
    if (lensChange) {
        if (self.countdownTimer) {
            [self.countdownTimer invalidate];
        }

        self.countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                               target:self
                                                             selector:@selector(calculateTimeLeft:)
                                                             userInfo:lensChange
     
                                                              repeats:YES];
        
        lblLensChangeMissed.hidden = YES;
    }
    }
}

-(void) calculateTimeLeft:(NSTimer *)timer{

    LensChange *lensChange = timer.userInfo;
    
    if ([lensChange.dateLensChange compare:[NSDate date]] == NSOrderedDescending) {
        lblTime.text = [self getTimeText:lensChange];
    }
    else{
        [self showLateNotification];
    }
    
    
}

-(void) showLateNotification
{
    if (self.countdownTimer) {
        [self.countdownTimer invalidate];
    }
    lblTime.text = @"Time to change lenses!";
    lblTime.textColor = [UIColor redColor];

    
}

-(NSString *) getTimeText:(LensChange *) lensChange{
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
    
    NSDateComponents *conversionInfo = [[NSCalendar currentCalendar] components:unitFlags fromDate:[NSDate date]  toDate:lensChange.dateLensChange  options:0];
    
    int months = [conversionInfo month];
    int days = [conversionInfo day];
    int hours = [conversionInfo hour];
    int minutes = [conversionInfo minute];
    int seconds = [conversionInfo second];
    
    return [NSString stringWithFormat:@"%d months , %d days, %d hours, %d min, %d second", months, days, hours, minutes, seconds];
}

- (LensChange *)loadLensChangeObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [defaults objectForKey:key];
    LensChange *obj = (LensChange *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
    return obj;
}

-(void) saveLensChangeObject:(LensChange *) lensChange withKey:(NSString *) key {
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:lensChange];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:myEncodedObject forKey:key];
    [defaults synchronize];    
}




-(IBAction) setupLensChangeNotifForDate:(NSDate *) date{

    //Remove any lens change notification set
    NSArray *array = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (UILocalNotification *localNotif in array) {
        NSDictionary *dict = localNotif.userInfo;
        NSString *lensChange = [dict objectForKey:@"CurrentLensChange"];
        if ([lensChange isEqualToString:@"CurrentLensChange"]) {
            [[UIApplication sharedApplication] cancelLocalNotification:localNotif];            
        }
    }

    //Add lens change notification
    UILocalNotification *localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
    localNotification.soundName=UILocalNotificationDefaultSoundName;
    [localNotification setFireDate:date]; //Set the date when the alert will be launched using the date adding the time the user selected on the timer
    [localNotification setTimeZone: [NSTimeZone localTimeZone]];
    [localNotification setAlertAction:@"EyePhoneApp"]; //The button's text that launches the application and is shown in the alert
    
    [localNotification setAlertBody:[NSString stringWithFormat: @"Time to change Lenses!"]];

    [localNotification setHasAction: YES]; //Set that pushing the button will launch the application
    [localNotification setApplicationIconBadgeNumber:[[UIApplication sharedApplication] applicationIconBadgeNumber]+1]; //Set the Application Icon Badge Number of the application's icon to the current Application Icon Badge Number plus 1
    [localNotification setUserInfo:[[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"CurrentLensChange",nil ] forKeys:[NSArray arrayWithObjects:@"CurrentLensChange",nil]]];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification]; //Schedule the notification with the system
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Lens change time setup successfully." delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
//    [alert show];
}


@end