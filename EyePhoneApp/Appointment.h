//
//  Appointment.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/31/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Appointment : NSObject
{
    NSDate *dateAppointment;

    BOOL finished;
}

@property (strong, nonatomic) NSDate *dateAppointment;
@property (strong, nonatomic) NSDate *dateReminder1;
@property (strong, nonatomic) NSDate *dateReminder2;

@end