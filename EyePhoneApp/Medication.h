//
//  Medication.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Medicine.h"

@interface Medication : NSObject
{
    NSInteger medicationId;
    NSString *name;
    NSDate *dateStart;
    NSDate *dateEnd;
    NSMutableArray *arrayMedicationTimes;
    Medicine *medicine;
    NSDate *dateLastTaken;
}
@property (nonatomic, assign) NSInteger medicationId;
@property (nonatomic, strong) Medicine *medicine;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *dateStart;
@property (nonatomic, strong) NSDate *dateEnd;
@property (nonatomic, strong) NSMutableArray *arrayMedicationTimes;
@property (nonatomic, strong) NSDate *dateLastTaken;
-(void) markMedicationAsTaken;
-(void) recordLastTakenTime;
-(void) markMedicationAsSkipped;

@end
