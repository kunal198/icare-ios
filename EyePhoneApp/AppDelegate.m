//
//  AppDelegate.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/7/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "AppDelegate.h"
#import "FMDBDataAccess.h"

@implementation AppDelegate
@synthesize databaseName, databasePath;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
        [NSThread sleepForTimeInterval:2.0];
    // Override point for customization after application launch.
    self.databaseName = @"EyeAppDB.sqlite";
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    self.databasePath = [documentDir stringByAppendingPathComponent:self.databaseName];
    
    [self createAndCheckDatabase];
    
    //reset all the medication times finished status if this is a new day
    
    NSDate *savedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"SavedDate"];
    
    if (savedDate == nil || [self daysBetweenDate:savedDate andDate:[NSDate date]] > 0) {
        NSLog(@"Reset");
        FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
        [fmdb resetAllMedications];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"SavedDate"];
    }
    
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentAppointment"];
    
    BOOL dontCancelAllNotifs = [[NSUserDefaults standardUserDefaults] boolForKey:@"FirstRun"];
    
    if (!dontCancelAllNotifs) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FirstRun"];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];            
    }
    
    //set the badge number to zero
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [[NSNotificationCenter defaultCenter] removeObserver: UIApplicationDidBecomeActiveNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(applicationEnteredForeground)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    
    

    if ([self.window respondsToSelector:@selector(setTintColor:)])
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        self.window.tintColor = [UIColor whiteColor];
        
    }
    
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
  
    
    
    
    
    
    
    return YES;
}


-(void) applicationEnteredForeground{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

-(void) createAndCheckDatabase
{
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:databasePath];
    
    if(success) return;
    
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseName];
    
    [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [[NSNotificationCenter defaultCenter] removeObserver: UIApplicationDidBecomeActiveNotification];

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

@end
