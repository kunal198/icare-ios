//
//  AppDelegate.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/7/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kLoadMap        1
#define kLoadWebsite    2
#define kLoadDoctors    3

#define IS_IPHONE ( [[[UIDevice currentDevice] model] isEqualToString:@"iPhone"] )
#define IS_IPOD   ( [[[UIDevice currentDevice ] model] isEqualToString:@"iPod touch"] )
#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
#define IS_IPHONE_5 ( IS_IPHONE && IS_HEIGHT_GTE_568 )

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSString *databaseName;
    NSString *databasePath;

}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString *databaseName;
@property (nonatomic, strong) NSString *databasePath;


@end
