//
//  VideosViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 11/17/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
@interface VideosViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *array;

@end
