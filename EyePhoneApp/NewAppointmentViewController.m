//
//  NewAppointmentViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "NewAppointmentViewController.h"


#define kTagAppointment     1001
#define kTagReminder1       1002
#define kTagReminder2       1003
#define kTagYear            1004

@interface NewAppointmentViewController ()

@end

@implementation NewAppointmentViewController
@synthesize appointment, years;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.appointment = [[Appointment alloc ] init];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    //Get Current Year into i2
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
    
    
    //Create Years Array from 1960 to This year
    self.years = [[NSMutableArray alloc] init];
    for (int i=i2; i<=3090; i++) {
        [self.years addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) close{
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(IBAction)actionFilter:(id)sender{

    
    if (sender == btnSetAppointment) {
        
        [pickerView removeFromSuperview];
        [picker removeFromSuperview];
        [toolBar removeFromSuperview];
       pickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width, 230)];
        pickerView.datePickerMode = UIDatePickerModeDateAndTime;
        [pickerView setMinimumDate: [NSDate date]];
        [self.view addSubview:pickerView];
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width,40)];
        [toolBar setBarStyle:UIBarStyleBlackOpaque];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(DonewithAppoinment)];
        toolBar.items = @[barButtonDone];
        pickerype = kTagAppointment;
        barButtonDone.tintColor=[UIColor whiteColor];
        [self.view addSubview:toolBar];
        
    }
    
    else if (sender == btnReminder1){
        pickerView.hidden = YES;
        picker.hidden = YES;
        [toolBar removeFromSuperview];
      picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width, 230)];
        picker.delegate = self;
        
        picker.showsSelectionIndicator = YES;
        pickerype = kTagReminder1;
        [self.view addSubview:picker];
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width,40)];
        [toolBar setBarStyle:UIBarStyleBlackOpaque];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(DonewithAppoinment)];
        toolBar.items = @[barButtonDone];
        barButtonDone.tintColor=[UIColor whiteColor];
        [self.view addSubview:toolBar];

    }
    else if (sender == btnReminder2){
        picker.hidden = YES;
        pickerView.hidden = YES;
        [toolBar removeFromSuperview];
        picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width, 230)];
        picker.delegate = self;
        pickerype = kTagReminder2;
        picker.showsSelectionIndicator = YES;
        [self.view addSubview:picker];
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width,40)];
        [toolBar setBarStyle:UIBarStyleBlackOpaque];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(DonewithAppoinment)];
        toolBar.items = @[barButtonDone];
        barButtonDone.tintColor=[UIColor whiteColor];
        [self.view addSubview:toolBar];


       
    }
    else if (sender == btnSetYear){
        picker.hidden = YES;
        pickerView.hidden = YES;
        [toolBar removeFromSuperview];
        picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width, 230)];
        picker.delegate = self;
         pickerype = kTagYear;
        picker.tag = kTagYear;
        picker.showsSelectionIndicator = YES;
        [self.view addSubview:picker];
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,300,self.view.frame.size.width,40)];
        [toolBar setBarStyle:UIBarStyleBlackOpaque];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(DonewithAppoinment)];
        toolBar.items = @[barButtonDone];
        barButtonDone.tintColor=[UIColor whiteColor];
        [self.view addSubview:toolBar];

        
    }
    
    
}
-(void)DonewithAppoinment
{
    if (pickerype == kTagAppointment)
    {
    [toolBar removeFromSuperview];
    pickerView.hidden = YES;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat: @"MMM-dd-yyyy, hh:mm a"];
    NSString *dateString = [dateFormatter stringFromDate: pickerView.date];
    [btnSetAppointment setTitle:dateString forState: UIControlStateNormal];
        [self.appointment setDateAppointment:pickerView.date];
    }
    else if (pickerype == kTagReminder1)
    {
        [toolBar removeFromSuperview];
        picker.hidden = YES;
        int hours = [picker selectedRowInComponent:0];
        NSString *string = [NSString stringWithFormat:@"%d Hours before.", ++hours];
        
        NSDateComponents *newStartDateComps = [[NSDateComponents alloc] init];
        [newStartDateComps setHour:-hours];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        [calendar setTimeZone:[NSTimeZone localTimeZone]];
        
        NSDate *reminderDate = [calendar dateByAddingComponents:newStartDateComps toDate:(NSDate *)appointment.dateAppointment options:0];
        
        //                        NSLog (@"Appointment: %@", appointment.dateAppointment);
        
        self.appointment.dateReminder1 = reminderDate;
        //                        NSLog (@"Reminder 1: %@", appointment.dateReminder1);
        
        
        [btnReminder1 setTitle:string forState: UIControlStateNormal];
    }
    else if (pickerype == kTagReminder2)
    {
        [toolBar removeFromSuperview];
        picker.hidden = YES;
        int hours = [picker selectedRowInComponent:0];
        NSString *string = [NSString stringWithFormat:@"%d Hours before.", ++hours];
        [btnReminder2 setTitle:string forState: UIControlStateNormal];
        
        NSDateComponents *newStartDateComps = [[NSDateComponents alloc] init];
        [newStartDateComps setHour:-hours];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        [calendar setTimeZone:[NSTimeZone localTimeZone]];
        
        NSDate *reminderDate = [calendar dateByAddingComponents:newStartDateComps toDate:(NSDate *)appointment.dateAppointment options:0];
        
        self.appointment.dateReminder2 = reminderDate;
        
    }
    else  if (pickerype == kTagYear)
    {
        [toolBar removeFromSuperview];
        picker.hidden = YES;
        NSNumber *newYear = [[NSNumber alloc] initWithInt:[[years objectAtIndex: [picker selectedRowInComponent:0]] intValue]];
        NSCalendar* gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        unsigned int unitFlags = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
        
        unsigned int hourlFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
        NSDate *currentDate = [self.appointment.dateAppointment copy];
        NSDateComponents* currentDateComponents = [gregorian components:hourlFlags fromDate:currentDate];
        NSDateComponents* dateComponents = [gregorian components:unitFlags fromDate:self.appointment.dateAppointment];
        [dateComponents setYear:[newYear intValue]];
        [dateComponents setHour:currentDateComponents.hour];
        [dateComponents setMinute:currentDateComponents.minute];
        [dateComponents setSecond:currentDateComponents.second];
        
        NSDate *newDate = [gregorian dateFromComponents:dateComponents];
        [self.appointment setDateAppointment:newDate];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat: @"MMM-dd-yyyy, hh:mm a"];
        
        NSString *dateString = [dateFormatter stringFromDate: newDate];
        [btnSetAppointment setTitle:dateString forState: UIControlStateNormal];
        
        [btnSetYear setTitle:[years objectAtIndex: [picker selectedRowInComponent:0]] forState: UIControlStateNormal];
        
        
        
        
        
        
        
        
        
    }
 }

#pragma mark -
#pragma  UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    int count = 0;
    
    if (pickerView.tag == kTagYear) {
        count = 10;
    }
    else{
        count = 24;
    }
    
    return count;
}

#pragma mark-
#pragma mark UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (picker.tag == kTagYear) {
        return [years objectAtIndex:row];
    }
    else {
        int index = row;
        return [NSString stringWithFormat:@"%d Hour(s)",++index];
    }
    
}

#pragma mark-


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [pickerView selectRow:row inComponent:component animated:YES];
}


-(IBAction) done {
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.appointment];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:myEncodedObject forKey:@"CurrentAppointment"];
    [defaults synchronize];
    
    NSArray *array = [NSArray arrayWithObjects:appointment.dateReminder1,appointment.dateReminder2, nil ];
    
   for (NSDate *date in array) {
    
    
    
        NSDateFormatter *Formatter = [[NSDateFormatter alloc] init];
        [Formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        NSString *Localtiming = [Formatter stringFromDate:date];
        [Formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        NSDate *localdate = [Formatter dateFromString:Localtiming];
        UILocalNotification *localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
        localNotification.soundName=UILocalNotificationDefaultSoundName;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];

        //[localNotification setTimeZone: [NSTimeZone localTimeZone]];
        NSLog(@"%@",date);
        [localNotification setFireDate:date]; //Set the date when the alert will be launched using the date adding the time the user selected on the timer
        [localNotification setAlertAction:@"EyePhoneApp"]; //The button's text that launches the application and is shown in the alert
        
        unsigned int unitFlags = NSHourCalendarUnit;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents *conversionInfo = [calendar components:unitFlags fromDate:date  toDate:appointment.dateAppointment  options:0];
        
        int hours = [conversionInfo hour];
        
        [localNotification setAlertBody:[NSString stringWithFormat: @"%d hours left before appointment!", hours]];
        NSLog (@"%@",[NSString stringWithFormat: @"%d hours left before appointment!", hours]);
        [localNotification setHasAction: YES]; //Set that pushing the button will launch the application
        [localNotification setApplicationIconBadgeNumber:[[UIApplication sharedApplication] applicationIconBadgeNumber]+1]; //Set the Application Icon Badge Number of the application's icon to the current Application Icon Badge Number plus 1
        [localNotification setUserInfo:[[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"CurrentAppointment",nil ] forKeys:[NSArray arrayWithObjects:@"CurrentAppointment",nil]]];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification]; //Schedule the notification with the system
    }
    
    [self.navigationController  popViewControllerAnimated:YES];

}

@end