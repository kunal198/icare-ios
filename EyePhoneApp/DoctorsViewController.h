//
//  DoctorsViewController.h
//  EyePhoneApp
//
//  Created by Arsalan Habib on 9/18/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
@interface DoctorsViewController : SuperViewController

-(IBAction) actionShowDoctorInfo:(id) sender;

@end