//
//  MedicationsViewController.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/22/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "MedicationsViewController.h"
#import "FMDBDataAccess.h"
#import "Medication.h"
#import "Event.h"
#import "NewMedicationViewController.h"

#define kCellLblMedication      1001
#define kCellLblDueAt           1002
#define kCellLblLastTaken       1003
#define kCellBtnTakeNow         1004
#define kCellLblLate            1005
#define kCellBtnSkip            1006
#define kCellBtnUndo            1007
#define kCellLblDueAtText       1008
#define kCellView               1009

#define kSegueNewMedicationViewController @"SegueNewMedicationViewController"

@interface MedicationsViewController ()

@end

@implementation MedicationsViewController
@synthesize arrayMedications;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if (IS_OS_7_OR_LATER)
    {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
    else if ([self.navigationController.navigationBar respondsToSelector:@selector(setTintColor:)])
        
    {
        
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.0/255.0 green:61.0/255.0 blue:43.0/255.0 alpha:1];
    }

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
    [[NSNotificationCenter defaultCenter] removeObserver: UIApplicationDidBecomeActiveNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(applicationEnteredForeground)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeTextColor : [UIColor whiteColor]};

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[NSNotificationCenter defaultCenter] removeObserver: UIApplicationDidBecomeActiveNotification];
    
}

-(void) applicationEnteredForeground{
    if (self.arrayMedications) {
        self.arrayMedications=nil;
    }
    FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
    self.arrayMedications = [fmdb getAllMedications];
    [self.tableView reloadData];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView setEditing:NO animated:YES];
    
    if (self.arrayMedications) {
        self.arrayMedications=nil;
    }
    FMDBDataAccess *fmdb = [[FMDBDataAccess alloc] init];
    self.arrayMedications = [fmdb getAllMedications];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    if (editing) {
        [self edit];
    }

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if([arrayMedications count] > 0){
        self.navigationItem.leftBarButtonItem.enabled = YES;
    }
    else{
        self.navigationItem.leftBarButtonItem.enabled = NO;
    }
    
    
    return [arrayMedications count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    UIButton *btnTakeNow = (UIButton *) [cell viewWithTag:kCellBtnTakeNow];
    [btnTakeNow addTarget:self action:@selector(takeNow:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnSkip = (UIButton *) [cell viewWithTag:kCellBtnSkip];
    [btnSkip addTarget:self action:@selector(skip:) forControlEvents:UIControlEventTouchUpInside];
    
    if (!editing) {
        btnTakeNow.alpha = 1;
        btnSkip.alpha = 1;
    }
    
    Medication *med = [arrayMedications objectAtIndex:indexPath.row];
    
    UILabel *lblMedicine = (UILabel *) [cell viewWithTag:kCellLblMedication];
    lblMedicine.text = med.medicine.name;
    
    UILabel *lblLastTaken = (UILabel *) [cell viewWithTag:kCellLblLastTaken];
    
    UILabel *lblLate = (UILabel *) [cell viewWithTag:kCellLblLate];
    [lblLate setHidden:YES];
    NSDate *dateNow = [NSDate date];
    
    UILabel *lblDueDate = (UILabel *) [cell viewWithTag:kCellLblDueAt];
    
    UILabel *lblDueAtText = (UILabel *) [cell viewWithTag:kCellLblDueAtText];
    
    BOOL boolMedicationDue = NO;
    
    BOOL boolMedicationSkipped = NO;
    
    
    for (Event *event in med.arrayMedicationTimes)
    {
        [lblDueAtText setText:@"Due at:"];
        [lblDueDate setHidden:NO];
        
        boolMedicationDue = NO;
        boolMedicationSkipped = NO;
        
        if (!event.finished) {
            boolMedicationDue = YES;
        }
        
        if (event.skipped) {
            boolMedicationSkipped = YES;
            continue;
        }
        
        unsigned int flags = NSHourCalendarUnit | NSMinuteCalendarUnit;

        NSCalendar* calendar = [NSCalendar currentCalendar];
        
        NSDateComponents* components = [calendar components:flags fromDate:event.dateEvent];
        
        NSDate* duetime = [calendar dateFromComponents:components];
        
        components = [calendar components:flags fromDate:dateNow];
        
        NSDate* nowtime = [calendar dateFromComponents:components];
        
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"hh:mm a"];
        
        NSString *time = [outputFormatter stringFromDate:event.dateEvent];
         lblDueDate.text = time;
        
        if (med.dateLastTaken) {
            [outputFormatter setDateFormat:@"E, hh:mm a"];
            NSString *lastTakenTime = [outputFormatter stringFromDate:med.dateLastTaken];
            lblLastTaken.text = lastTakenTime;
        }
        
        // If the due time is less than the current time AND the due time is not finished AND this medication has not been skipped, then show this time and show LATE and show take now and skip buttons
        if (([duetime compare:nowtime] == NSOrderedAscending || [duetime compare:nowtime] == NSOrderedSame )  && !event.finished) {
           
            [lblLate setHidden:NO];            
            [btnSkip setHidden:NO];
            [btnTakeNow setHidden:NO];
            
            goto outer;
        }
        
        // If the due time is greater than the current time, then show this time as the next due time and don't show the take now or skip buttons
        else if ([duetime compare:nowtime] == NSOrderedDescending ) {

            [lblLate setHidden:YES];
            [btnSkip setHidden:YES];
            [btnTakeNow setHidden:YES];
            [lblDueAtText setText:@"Due at:"];
            [lblDueDate setHidden:NO];
            goto outer;
        }
        
        
       
    }

outer:
    
    if (!boolMedicationDue) {
        [lblDueDate setHidden:YES];
        [lblDueAtText setText:@"Taken"];
        [lblLate setHidden:YES];
        [btnSkip setHidden:YES];
        [btnTakeNow setHidden:YES];
    }
    
    if (boolMedicationSkipped) {
        [lblDueDate setHidden:YES];
        [lblDueAtText setText:@"Skipped"];
        [lblLate setHidden:YES];
        [btnSkip setHidden:YES];
        [btnTakeNow setHidden:YES];
    }

   
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - Methods

-(void) takeNow:(id) sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    Medication *med = [arrayMedications objectAtIndex:indexPath.row];
    [med markMedicationAsTaken];
    //record last taken time
    [med recordLastTakenTime];
    
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

-(void) skip:(id) sender{
    UIView *contentView = [sender superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Medication *med = [arrayMedications objectAtIndex:indexPath.row];
    [med markMedicationAsSkipped];
    //do not record taken
    
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}


-(IBAction) edit{
    if (!editing) {
        [self.tableView setEditing:YES animated:YES];
        editing = YES;
    }
    else{
        [self.tableView setEditing:NO animated:YES];
        editing = NO;
        [self.tableView reloadData];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//    UIView *view = (UIView *) [cell viewWithTag:kCellView];
//    
//    CGRect frame = view.frame;
//    frame.origin.x += 150;
//    view.frame = frame;
    UIButton *btnTakeNow = (UIButton *) [cell viewWithTag:kCellBtnTakeNow];
    UIButton *btnSkip = (UIButton *) [cell viewWithTag:kCellBtnSkip];
    btnTakeNow.alpha = 0.5;
    btnSkip.alpha = 0.5;

    
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSLog(@"Delete row %d", indexPath.row);
        
        Medication *medication = [self.arrayMedications objectAtIndex:indexPath.row];
        
        FMDBDataAccess *fmdb  = [[FMDBDataAccess alloc] init];
        
        BOOL result = [fmdb deleteMedication:medication];
        
        if (result) {
            [self.arrayMedications removeObject:medication];
            
            [self.tableView reloadData];
            
            NSArray *array = [[UIApplication sharedApplication] scheduledLocalNotifications];
            
            for (UILocalNotification *localNotif in array) {
                NSDictionary *dict = localNotif.userInfo;
                NSNumber *medicationId = [dict objectForKey:@"medication"];
                if ([medicationId intValue] == medication.medicationId) {
                    NSLog (@"%d",medication.medicationId);
                    [[UIApplication sharedApplication] cancelLocalNotification:localNotif];

                }
            }
            
            
            
        }
    }
}


-(IBAction) actionAddNewMedication{
    [self performSegueWithIdentifier:kSegueNewMedicationViewController  sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //You can access the tableviewcontroller using segue.destinationViewController
    //Maybe you want to set your model here.
    if ([segue.identifier isEqualToString:kSegueNewMedicationViewController]) {
        NewMedicationViewController *vc  = segue.destinationViewController;
        vc.delegate = sender;
    }
}


@end
