//
//  Appointment.m
//  EyePhoneApp
//
//  Created by Arsalan Habib on 8/31/13.
//  Copyright (c) 2013 Softack. All rights reserved.
//

#import "Appointment.h"

@implementation Appointment
@synthesize dateAppointment, dateReminder1, dateReminder2;

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.dateAppointment forKey:@"dateAppointment"];
    [encoder encodeObject:self.dateReminder1 forKey:@"dateReminder1"];
    [encoder encodeObject:self.dateReminder2 forKey:@"dateReminder2"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.dateAppointment = [decoder decodeObjectForKey:@"dateAppointment"];
        self.dateReminder1 = [decoder decodeObjectForKey:@"dateReminder1"];
        self.dateReminder2 = [decoder decodeObjectForKey:@"dateReminder2"];
    }
    return self;
}

@end
